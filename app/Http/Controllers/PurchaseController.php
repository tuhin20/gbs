<?php


namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use App\Models\Brand;
use App\Models\Vendor;
use App\Models\AttributeGroup;
use Auth;
use DB;
use App\Models\Purchase;
use App\Models\Purchase_item;


class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('purchase.addpurchase');
    }
    public function get_purchase(Request $request) {
        $search = $request->search;

        if ($search == '') {
            $purchase_list = Purchase::orderby('id', 'asc')->limit(5)->get();
        } else {
            $purchase_list = Purchase::orderby('id', 'asc')->where('name', 'like', '%' . $search . '%')->limit(10)->get();
        }

        $response = array();
        foreach ($purchase_list as $purchase) {
            $response[] = array("value" => $purchase->id, "label" => $purchase->name);
        }

        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //dd($request->all());
  /* $request->validate([
            'purchase_no' => 'required',
           
            'sale_date' => 'required',
           
        ]);*/
       $purchase = new Purchase();
       $purchase->purchase_no ='123';
       $purchase->store_id ='123';
       $purchase->supplier_id ='123';
       $purchase->tax = '7676';
       $purchase->sub_total = $request->sub_total;
       $purchase->discount_percent = $request->discount_percent;
       $purchase->discount_overall = $request->discount_overall;
       $purchase->grand_total = $request->grand_total;
       $purchase->sale_date = $request->sale_date;
       $purchase_id=$purchase->id;
       $purchase=$purchase->save();
/*start purchase item*/

    /*    $item_price=$request->item_price;
        $item_qty=$request->item_qty;
        $item_discount_percent=$request->item_discount_percent;
        $item_total_pirce=$request->item_total_pirce;
        for ($i=0; $i <count($item_price); $i++) { 
        $datasave=[
        'product_id'=>1,
        'item_price'=>$item_price,
        'item_qty'=>$item_qty, 
        'item_discount_percent'=>$item_discount_percent,
        'item_total_pirce'=>$item_total_pirce
        ];
        DB::table('purchase_items')->insert($datasave);

        }*/
 //dd($product_id);
        
   foreach ($product_id as $key => $value){
       
       $purchase_items = new Purchase_item();
       $purchase_items->product_id=$value;
       $purchase_items->item_price='66';
       $purchase_items->item_qty=$item_qty[$key];
       $purchase_items->item_discount_percent =$item_discount_percent[$key];
       $purchase_items->item_total_pirce=$item_total_pirce[$key];
       $purchase_items->save();
  }
/*=================             
 foreach ($expense_name as $key => $value){
                  
                  $expense = New Expense;
                  $expense->expense_name = $value;
                  $expense->expense_date = $expense_date;
                  $expense->description = $description[$key];
                  $expense->expense_amount = $expense_amount[$key];
                  $expense->save();
              }
============*/


      /* echo "<pre>";
       print_r($purchase);die();*/
     //  dd($purchase);
       return back();
   }

    public function get_supplier(Request $request) {
        $search = $request->search;

        if ($search == '') {
            $vendor_list = Vendor::orderby('id', 'asc')->limit(5)->get();
        } else {
            $vendor_list = Vendor::orderby('id', 'asc')->where('vendor_name', 'like', '%' . $search . '%')->limit(10)->get();
        }

        $response = array();
        foreach ($vendor_list as $vendor) {
            $response[] = array("value" => $vendor->id, "label" => $vendor->vendor_name);
        }

        return response()->json($response);
    }
public function get_supplier_by_id(Request $request)
    {
       $id = $request->vendor_id;
       $vendor_info = Vendor::find($id);
       return response()->json($vendor_info);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function show(Purchase $purchase)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function edit(Purchase $purchase)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Purchase $purchase)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function destroy(Purchase $purchase)
    {
        //
    }
}
