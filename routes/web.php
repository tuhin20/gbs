<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();



Route::group(['middleware' => 'auth'], function () {
    // Authentication Routes...
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('/category','CategoryController');
    Route::resource('/sub_category','SubCategoryController');
    Route::resource('/customer','CustomerController');
    Route::post('customer-list', 'CustomerController@customer_list');
    Route::get('get_customer', 'CustomerController@get_customer');
    //Users
    Route::post('user-list', 'UserController@user_list');
    Route::resource('users', 'UserController');
    Route::resource('categories', 'CategoryController');
    Route::resource('brands', 'BrandController');
    //Attributes
    Route::resource('attributes', 'AttributeController');
    //Get Attribute Group By Id
    Route::post('fetch-attribute-group/{any}', 'AttributeController@fetch_attribute_group');
    //Delete Attribute  By Id
    Route::delete('delete-attribute/{any}', 'AttributeController@delete_attribute');
    //update Attribute By Id
    Route::post('attributes-update/{any}', 'AttributeController@attributes_update');

    Route::resource('products', 'ProductController');
    Route::post('products/upload', 'ProductController@upload')->name('products.upload');
    Route::get('get-products', 'ProductController@get_products');
    Route::get('get-product-by-id', 'ProductController@get_product_by_id');
     //Upload Summernote Image
    Route::post('summurnote-image-upload', 'AttributeController@summurnote_image_upload');
    Route::resource('units', 'UnitController');
    Route::resource('pos','PosController');
    //Role & Permission
    Route::resource('roles', 'RoleController');
    Route::resource('permissions', 'PermissionController');
//Purchase start
Route::resource('purchase','PurchaseController');
Route::get('get-purchase', 'PurchaseController@get_purchase');
Route::get('get-supplier', 'PurchaseController@get_supplier');
 Route::get('get-supplier-by-id', 'PurchaseController@get_supplier_by_id');
//Route::post('store', 'PurchaseController@store');
    //purchase end

});

